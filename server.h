/**
* @file server.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>

/**
* Connection held by a single thread
*/
void *Connection(void *client_data);

/**
* Add given movieFile to the global list (thread-safe)
*/
void addMovie(MovieFile movie);

/**
* Add given client to the global list (thread-safe)
*/
void addClient(Client client);

/**
* Looks in the movie list and finds a movie by name
*/
MovieFile findMovie(char* name);

/**
* Finds the ideal seeder that has the given frame for given movie
* Warning: frame numbers start from 1 (not 0)
*/
Client whoHasFrame(char* movieName, int frameID);

/**
* printm is a thread-safe printing function
*/
void printm(char* message);

/**
* Prints global stats for the tracking server
*/
void printTrackerStats();

/**
* Print a help message
*/
void help();
