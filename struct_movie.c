/**
* @file struct_movie.c
* @author Thomas Lextrait
*/

#include "struct_movie.h"
#include "utils.h"

#define MAX_FILENAME_LEN	256
#define MAX_LINE_LEN		10240
#define MAX_FRAME_COUNT		1024

/**
* Opens an ASCII movie file and fills out the information
* for a MovieFile struct
*/
void ReadMovieFile(char* filename, MovieFile* movieFile)
{
	char* line = (char*)malloc(sizeof(char) * MAX_LINE_LEN);
	char* param = (char*)malloc(sizeof(char) * MAX_LINE_LEN); // for reading movie parameters
	int frameCountLegacy = 0; // support for old movie files
	
	// Copy filename
	strcpy(movieFile->filename, extractFilename(filename));
	
	// Copy movie name
	strcpy(movieFile->movieName, extractFilename(filename));
	stripFileExtension(movieFile->movieName);
	
	// Initialize other info
	memset(movieFile->address, '\0', 20);
	movieFile->FPS = 4;	// default FPS is 4
	movieFile->originalFrameCount = -1;
	movieFile->currentFrameCount = 0;
	movieFile->movieType = movietype_rich;
	movieFile->seederCount = 0;
	movieFile->seedingPort = 6666;
	
	// Open file
	FILE* file = (FILE*)malloc(sizeof(FILE));
	file = fopen(filename, "r");
	
	while(!isMovieEnd(line)){
		
		fgets(line, MAX_LINE_LEN, file);
		
		// Get FPS
		if(getMovieParameter(line, "#frame_rate[", param)){
			movieFile->FPS = atoi(param);
		}
		
		// Get original frame count
		if(getMovieParameter(line, "#original_frame_count[", param)){
			movieFile->originalFrameCount = atoi(param);
		}
		
		// Count current frame count
		if(getMovieParameter(line, "#frame_num[", param)){
			// Add frame to array of frames availables
			movieFile->framesAvailable[movieFile->currentFrameCount] = atoi(param);
			movieFile->currentFrameCount++;
		}
		
		if(isFrameEnd(line)){
			movieFile->framesAvailable[frameCountLegacy] = atoi(param);
			frameCountLegacy++;
		}
	}
	
	// No current frame count?
	if(movieFile->currentFrameCount <= 0){
		movieFile->currentFrameCount = frameCountLegacy;
		movieFile->movieType = movietype_legacy;
	}
	
	// No original frame count?
	if(movieFile->originalFrameCount <= 0){
		movieFile->originalFrameCount = movieFile->currentFrameCount;
		movieFile->movieType = movietype_legacy;
	}
	
	// Compute duration and frame availability
	movieFile->duration = ((int)((double)movieFile->originalFrameCount / (double)movieFile->FPS * 100.0))/100.0;
	movieFile->availability = ((int)(10000.0*(double)movieFile->currentFrameCount / (double)movieFile->originalFrameCount))/100.0;
	
	// Clean up
	fclose(file);
	free(line);
	free(param);
}

/**
* Returns 1 if value has been found, 0 otherwise.
* 	Movie parameters are in the format: #parameter_name[value]
*	For this parameter, pass in "#parameter_name[" for char* parameter
*	Don't forget to include the opening square bracket when passing to this function
*/
int getMovieParameter(char* line, char* parameter, char* value)
{
	int i = strlen(parameter), a = 0;
	if(startsWith(line, parameter)){
		line += i; // go straight to number
		while(*line != ']'){
			*value = *line;
			value++;
			line++;
			i++;
			a++;
		}
		*value = '\0';
		line -= i;
		value -= a;
		return 1;
	}else{
		return 0;
	}
}

/**
* Opens movie file and saves the requested frame on char* frame
*/
int getFrame(char* filename, int frameID, char* frame)
{
	char* line = (char*)malloc(sizeof(char) * MAX_LINE_LEN);
	char* param = (char*)malloc(sizeof(char) * MAX_LINE_LEN);
	int found = 0;
	// Open file
	FILE* file = (FILE*)malloc(sizeof(FILE));
	file = fopen(filename, "r");
	
	while(!isMovieEnd(line)){
		
		fgets(line, MAX_LINE_LEN, file);
		
		// Count current frame number
		if(getMovieParameter(line, "#frame_num[", param) && atoi(param) == frameID){
			break;
		}
	}
	
	if(!isMovieEnd(line)){
		while(!isFrameEnd(line) && fgets(line, MAX_LINE_LEN, file)!=NULL){
			found = 1;
			if(!isFrameEnd(line)){
				frame = strcat(frame, line);
			}
		}
	}
	
	fclose(file);
	free(line);
	free(param);
	
	return found;
}

/**
* Returns 1 if given line is a frame end, 0 otherwise
*/
int isFrameEnd(char* line)
{
	if(strcmp(line, "end")==0 || 
		strcmp(line, "end\n")==0 ||
		strcmp(line, "end\r")==0 ||
		strcmp(line, "end\r\n")==0
	){
		return 1;
	}else{
		return 0;
	}
}

/**
* Returns 1 if given line is a movie end, 0 otherwise
*/
int isMovieEnd(char* line)
{
	if(strcmp(line, "stop")==0 || 
		strcmp(line, "stop\n")==0 ||
		strcmp(line, "stop\r")==0 ||
		strcmp(line, "stop\r\n")==0
	){
		return 1;
	}else{
		return 0;
	}
}

/**
* Prints the data in a MovieFile struct
* -- For testing purposes only --
*/
void printMovieStruct(MovieFile movieFile)
{
	printf("MovieFile->address\t\t%s\n", movieFile.address);
	printf("MovieFile->filename\t\t%s\n", movieFile.filename);
	printf("MovieFile->movieName\t\t%s\n", movieFile.movieName);
	printf("MovieFile->originalFrameCount\t%d\n", movieFile.originalFrameCount);
	printf("MovieFile->currentFrameCount\t%d\n", movieFile.currentFrameCount);
	printf("MovieFile->FPS\t\t\t%d\n", movieFile.FPS);
	printf("MovieFile->type\t\t\t%d\n", movieFile.movieType);
}
