/**
* @file client.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <stdlib.h> 	// system(clear)
#include <string.h>
#include <unistd.h>		// getopt()
#include <time.h>

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef struct FrameRequest{
	char movieName[128];
	int frameID;
}FrameRequest;

/**
* Start the movie leeching routine
*/
void startLeeching(char* address, int port);

/**
* Start the movie seeding routing
*/
void startSeeding(char* address, int port);

/**
* Tells the tracker that we're a leecher
*/
void identifyAsLeecher(int socket);

/**
* Tells the tracker that we're a seeder
*/
void identifyAsSeeder(int socket);

/**
* Tells the tracker that we're a killer
*/
void identifyAsKiller(int socket);

/**
* Prints a slider representing movie progress
*/
void printSlider(int current, int max);

/**
* Accept a connection on given socket, returns a new socket
*/
int acceptConnection(int sock);

/**
* Waits until peer is ready
*/
void waitForReady(int sock);

/**
* Submits given frame to a leecher
*/
void handleFrameRequest(int sock, FrameRequest* req, MovieFile movies[], int movieCount);

/**
* Requests a movie frame from a seeder and prints it
*/
void requestFrame(char* address, int port, char* movieName, int frameID);

/**
* Binds to local address to let others connect to this program
* return a socket descriptor
*/
int bindLocalAddress(int port);

/**
* Creates a connection to tracking server and returns socket descriptor
*/
int connectToTracker(char* address, int port);

/**
* Print a help message
*/
void help();
