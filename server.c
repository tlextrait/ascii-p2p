/** 
 * @file server.c
 * @author Thomas Lextrait
 */

#include "struct_movie.h"
#include "struct_client.h"
#include "utils.h"
#include "server.h"

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

#define SERVER_PORT 		6666
#define MAX_CONNECTIONS 	500
#define MSG_SIZE 			10240		// 10KB
#define MAX_THREADS			500
#define MAX_MOVIES			500
#define MAX_NAME_LEN		128
#define MAX_MOVIE_FRAMES	2048

/**
* GLOBAL VARIABLES
*/
int specPort = -1;              // Specify port
int globalClientCount = 0;      // Client count
int globalSeederCount = 0;      // Seeder count
int globalMovieFileCount = 0;   // Movie file count
int globalMovieCount = 0;       // Unique movie count
int killMaster = 0;             // Switch for killing the server

Client				clients[MAX_THREADS];       // Array of seeders
MovieFile			movieFiles[MAX_MOVIES];		// Array of movie files
MovieFile			movieList[MAX_MOVIES];		// Array of unique movies

pthread_t 			threads[MAX_THREADS];       // Array of threads
pthread_mutex_t		mutexPrint;                 // Mutex for printing to screen
pthread_mutex_t		mutexClients;               // Mutex for modifying client array
pthread_mutex_t		mutexMovies;                // Mutex for modifying movie array
pthread_mutex_t		mutexKill;                  // Mutex for kill switch
pthread_attr_t 		attr;                       // thread attributes

int main(int argc, char *argv[])
{
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Socket stuff
	int sock, newsock, tcp_port, bytes;
	unsigned int clilen;
	struct sockaddr_in cli_addr, serv_addr;
	char* message = (char*)malloc(sizeof(char)*MSG_SIZE);
	
	// Thread stuff
	int rc, i;
	int currentThreadID = 0;
	
	/* ------------------------------------------------------------------------ */

	// Parse command line options
	while((c = getopt (argc, argv, "hp:")) != EOF) {
	    switch(c){
			case 'p': // specify port
				tcp_port = atoi(optarg);
				specPort = 0;
				break;
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}

	// DEFAULT Port
	if(specPort != 0){tcp_port = SERVER_PORT;}
	
	// Create socket from which to read
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	   perror("Can't open stream socket.\n");
	   exit(1);
	}

	// bind our local address so client can connect to us
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(tcp_port);
	if(bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("Can't bind to local address.\n");
		exit(1);
	}

	printf("Server accepting connections.\n");

	// mark the socket as passive, and sets backlog num
	listen(sock, MAX_CONNECTIONS);
	
	// Initialize threading
	pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_mutex_init(&mutexPrint, NULL);
	pthread_mutex_init(&mutexClients, NULL);
	pthread_mutex_init(&mutexMovies, NULL);
	pthread_mutex_init(&mutexKill, NULL);
    
    // While the kill switch is off
	while(!killMaster){
		
		clilen = sizeof(cli_addr);
		newsock = accept(sock, (struct sockaddr *) &cli_addr, &clilen);
		if (newsock < 0) {
			perror("Error while accepting connection.\n");
			break;
		}
		printf("\nConnection request received.\n");
		
		// Create the client
		initializeClient(&clients[currentThreadID], newsock);
		
		if((bytes = read(clients[currentThreadID].socket, message, MSG_SIZE)) < 0){
			printm("Error in read.\n");
			pthread_exit((void*) 0);
		}
		
        // Identify client
		if(strcmp(message, "leech")==0){
			clients[currentThreadID].clientType = client_leecher;
		}else if(strcmp(message, "seed")==0){
			clients[currentThreadID].clientType = client_seeder;
		}else if(strcmp(message, "kill")==0){
			printf("Shutting down...\n");
			break;
		}else{
			printf("Could not identify client type [leecher|seeder].\n");
		}
		
		if(
			clients[currentThreadID].clientType == client_leecher || 
			clients[currentThreadID].clientType == client_seeder
		){
			rc = pthread_create(&threads[currentThreadID], &attr, Connection, (void*) &clients[currentThreadID]);
			currentThreadID++;

			if(rc){
	        	printf("Error: pthread_create() failed and returned %d\n", rc);
				break;
	        }
		}
	}
	
	// Join threads
	for(i=0; i<currentThreadID; i++){
		rc = pthread_join(threads[i], NULL);
	}
	
	close(sock);
	pthread_mutex_destroy(&mutexPrint);
	pthread_mutex_destroy(&mutexClients);
	pthread_mutex_destroy(&mutexMovies);
	pthread_mutex_destroy(&mutexKill);
    pthread_attr_destroy(&attr);
	free(message);

	// Tell each seeder to die
	for(i=0; i<globalClientCount; i++){
		if(clients[i].clientType == client_seeder){
			printf("Killing: %s:%d\n", clients[i].address, clients[i].port);
			killClient(clients[i]);
		}
	}

    pthread_exit(NULL);
}

/**
* Connection held by a single thread
*/
void *Connection(void *client_data)
{
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	int bytes = 0, i;
	char* message = (char*)malloc(sizeof(char)*MSG_SIZE);
	MovieFile* movies = (MovieFile*)malloc(sizeof(MovieFile) * MAX_MOVIES);
	MovieFile movieRequested;
	
	// For each movie frame, map a client
	Client framesClient[MAX_MOVIE_FRAMES];
	
	// For fetching IP address of client
	struct sockaddr_in m_addr;
	socklen_t len = sizeof(m_addr);
	
	/* ------------------------------------------------------------------------ */
	
	// Copy the client struct
	Client *client = (Client*)client_data;
	
	// Get IP address of client
	if(getpeername(client->socket, (struct sockaddr*)&m_addr, &len) < 0){
		printm("\nCould not get IP address of client.\n");
		printm("\nClosing socket, exitting thread.\n");
		close(client->socket);
		pthread_exit((void*) 0);
	}
	strcpy(client->address, inet_ntoa(m_addr.sin_addr));
	
	// Identify client type
	if(client->clientType == client_leecher){
		/* ------------------------------------------------------------------------ */
		/*									LEECHER									*/
		/* ------------------------------------------------------------------------ */
		printm("Leecher: ");
		printm(client->address);
		printm("\n");
		
		client->clientType = client_leecher;
		
		// Give client the movie listing
		if(write(client->socket, movieList, sizeof(MovieFile) * globalMovieCount) <= 0){
			perror("Error sending list of movies to leecher.");
			close(client->socket);
			pthread_exit((void*) 0);
		}
		
		// Wait for movie name request
		if((bytes = read(client->socket, message, sizeof(char) * MAX_NAME_LEN)) < 0){
			perror("Error receiving movie listing from tracker.");
			close(client->socket);
			pthread_exit((void*) 0);
		}
        
        if(strlen(message) > 0){
            
            printm("Movie requested '");
            printm(message);
            printm("'\n");
            
            // Find the movie
            movieRequested = findMovie(message);
            
            if(movieRequested.originalFrameCount > 0){

                printm("Movie found.\n");
                
                if(movieRequested.originalFrameCount > 0){
                    // For each frame, specify the seeder to contact
                    for(i=0; i<movieRequested.originalFrameCount; i++){
                        framesClient[i] = whoHasFrame(movieRequested.movieName, i+1);
                    }
                }
                
                // Send digest to client
                if(write(client->socket, framesClient, sizeof(Client) * MAX_MOVIE_FRAMES) <= 0){
                    close(client->socket);
                    pthread_exit((void*) 0);
                    perror("Error sending digest to leecher.");
                }
            }else{
                printm("Movie not found.\n");
            }
            
        }
		
		
	}else if(client->clientType == client_seeder){
		/* ------------------------------------------------------------------------ */
		/*									SEEDER									*/
		/* ------------------------------------------------------------------------ */
		printm("Seeder: ");
		printm(client->address);
		printm("\n");
		
		client->clientType = client_seeder;
		
		// tell seeder we're ready to get the movie list
		if(write(client->socket, "ready", 6) <= 0){
			perror("Error telling the seeder we're ready to receive movie listing.");
			close(client->socket);
			pthread_exit((void*) 0);
		}
		
		if((bytes = read(client->socket, movies, sizeof(MovieFile) * MAX_MOVIES)) < 0){
			printm("Error in read.\n");
			pthread_exit((void*) 0);
		}
		
		i=0;
		while(movies->movieType > 0){
			strcpy(movies->address, client->address);	// save IP address into movie struct
			addMovie(*movies);
			movies++;
			i++;
		}
		movies-=i;
		
		printm("Received ");
		printm(itoa(i));
		printm(" movies\n");
		
		// Tell seeder we're ready to get the random client port
		if(write(client->socket, "ready", 6) <= 0){
			perror("Error telling the seeder we're ready to receive random port.");
			close(client->socket);
			pthread_exit((void*) 0);
		}
		
		/*
		* Server needs to know the port of every seeder in order to be able to 
		* kill them when shutting down
		*/
		if((bytes = read(client->socket, &(client->port), MSG_SIZE)) < 0){
			printm("Error in read.\n");
			pthread_exit((void*) 0);
		}
		
		//client->port = atoi(message);
		close(client->socket);
		addClient(*client);
		printTrackerStats();
	}
	
	// Cleanup
	close(client->socket);
	free(message);
	free(movies);
	pthread_exit((void*) 0);
}

/**
* Add given movieFile to the global list (thread-safe)
*/
void addMovie(MovieFile movie)
{
	int i = 0, found = 0;
	pthread_mutex_lock(&mutexMovies);
	
	// Add movie file
	movieFiles[globalMovieFileCount] = movie;
	globalMovieFileCount++;
	
	// Add movie only if not already in the list
	for(i=0; i<globalMovieCount; i++){
		if(strcmp(movieList[i].movieName, movie.movieName)==0){
			found = 1;
			movieList[i].seederCount++;
		}
	}
	
	if(!found){
		movieList[globalMovieCount] = movie;
		movieList[globalMovieCount].seederCount = 1;
		globalMovieCount++;
	}
	
	pthread_mutex_unlock(&mutexMovies);
}

/**
* Add given client to the global list (thread-safe)
*/
void addClient(Client client)
{
	int found = 0, i;
	
	pthread_mutex_lock(&mutexClients);
	
	// Add seeder only if not already in the list
	for(i=0; i<globalClientCount; i++){
		if(
			strcmp(clients[i].address, client.address)==0 && 
			clients[i].port == client.port
		){found = 1;}
	}
	if(!found){
		client.clientID = globalClientCount+1; // assign unique client ID
		clients[globalClientCount] = client;
		globalClientCount++;
		if(client.clientType == client_seeder){
			globalSeederCount++;
		}
	}
	
	pthread_mutex_unlock(&mutexClients);
}

/**
* Looks in the movie list and finds a movie by name
*/
MovieFile findMovie(char* name)
{
	int i;
	MovieFile empty;
	empty.originalFrameCount = 0;
	for(i=0; i<globalMovieCount; i++){
		if(strcmp(movieList[i].movieName, name) == 0){
			return movieList[i];
		}
	}
	return empty;
}

/**
* Finds the ideal seeder that has the given frame for given movie
* Warning: frame numbers start from 1 (not 0)
*/
Client whoHasFrame(char* movieName, int frameID)
{
	int i, a;
	Client client;
	client.port = -1;
	
	for(i=0; i<globalMovieFileCount; i++){
		// Is it the right movie?
		if(strcmp(movieFiles[i].movieName, movieName) == 0){
			// Is the frame there?
			for(a=0; a<movieFiles[i].currentFrameCount; a++){
				if(movieFiles[i].framesAvailable[a] == frameID){
					strcpy(client.address, movieFiles[i].address);
					client.port = movieFiles[i].seedingPort;
					return client;
				}
			}
		}
	}
	return client;
}

/**
* printm is a thread-safe printing function
*/
void printm(char* message)
{
	pthread_mutex_lock(&mutexPrint);
	printf("%s", message);
	pthread_mutex_unlock(&mutexPrint);
}

/**
* Prints global stats for the tracking server
*/
void printTrackerStats()
{
	pthread_mutex_lock(&mutexPrint);
	printf("------------------------------------------\n");
	printf("Total movie files tracked:\t%d\n", globalMovieFileCount);
	printf("Total movies tracked:\t\t%d\n", globalMovieCount);
	printf("Total seeders:\t\t\t%d\n", globalSeederCount);
	printf("------------------------------------------\n");
	pthread_mutex_unlock(&mutexPrint);
}

/**
* Print a help message
*/
void help()
{
	printf("server - thomas.lextrait@gmail.com\n");
	printf("usage: server [-h] [-p<port>]\n");
	printf("\t-p\tSpecify a port, otherwise will use 6666.\n");
	printf("\t-h\tDisplay this help message.\n");
}

