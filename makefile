#######################################
# Thomas Lextrait , tlextrait@wpi.edu
#######################################
CC=gcc
CFLAGS=-c -Wall
UTILSH=utils.h struct_client.h struct_movie.h
UTILSO=utils.o struct_client.o struct_movie.o
THREAD=-pthread
#######################################

all: client server

server: server.o $(UTILSO)
	$(CC) $(THREAD) server.o $(UTILSO) -o server
	
server.o: server.c server.h $(UTILSH)
	$(CC) $(CFLAGS) server.c

client: client.o $(UTILSO)
	$(CC) client.o $(UTILSO) -o client

client.o: client.c client.h $(UTILSH)
	$(CC) $(CFLAGS) client.c
	
#######################################
# Structures
#######################################
struct_movie.o: struct_movie.c struct_movie.h
	$(CC) $(CFLAGS) struct_movie.c

struct_client.o: struct_client.c struct_client.h
	$(CC) $(CFLAGS) struct_client.c

#######################################
# Utilities
#######################################	
utils.o: utils.c utils.h struct_movie.h
	$(CC) $(CFLAGS) utils.c

#######################################
# Clean up
#######################################
clean:
	rm -f *.o *.out client server

superclean:
	rm -f *.o *.out *~ client server
	