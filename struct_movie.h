/**
* @file struct_movie.h
* @author Thomas Lextrait
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum MovieType{
	movietype_null = 0,
	movietype_legacy = 1,
	movietype_rich = 2		// rich movie has parameters (FPS, frame count etc.)
}MovieType;

typedef struct MovieFile{
	char address[20];			// IP address of the owner (information populated by the server)
	int seedingPort;			// Port at which seeder is seeding this movie
	int seederCount;			// How many seeders have this movie? -- this is only set by tracker in the global movie list --
	
	char filename[128];			// Name of file for the movie
	char movieName[128];		// Name of the movie
	int originalFrameCount;		// Number of frames in original movie
	int currentFrameCount;		// Number of frames in this file
	int FPS;					// Original frame rate in frames per second
	int framesAvailable[2048];	// Array containing the frame# for each available frame
	double duration;			// Movie duration in seconds
	double availability;		// Frame availability in %
	MovieType movieType;
}MovieFile;

/**
* Opens an ASCII movie file and fills out the information
* for a MovieFile struct
*/
void ReadMovieFile(char* filename, MovieFile* movieFile);

/**
* Returns 1 if value has been found, 0 otherwise.
* 	Movie parameters are in the format: #parameter_name[value]
*	For this parameter, pass in "#parameter_name[" for char* parameter
*	Don't forget to include the opening square bracket when passing to this function
*/
int getMovieParameter(char* line, char* parameter, char* value);

/**
* Opens movie file and saves the requested frame on char* frame
*/
int getFrame(char* filename, int frameID, char* frame);

/**
* Returns 0 if given line is a frame end, 1 otherwise
*/
int isFrameEnd(char* line);

/**
* Returns 0 if given line is a movie end, 1 otherwise
*/
int isMovieEnd(char* line);

/**
* Prints the data in a MovieFile struct
* -- For testing purposes only --
*/
void printMovieStruct(MovieFile movieFile);
