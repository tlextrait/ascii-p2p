/**
* @file client.c
* @author Thomas Lextrait
*/

#include "struct_client.h"
#include "struct_movie.h"
#include "client.h"
#include "utils.h"

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

#define MAX_INPUT_LENGTH		128
#define MAX_MSG_SIZE			20480

#define DEFAULT_PORT			6666
#define DEFAULT_ADDRESS			"192.168.1.108"
#define MAX_CONNECTIONS			500

#define MOVIE_FOLDER 			"movies/"
#define MAX_FILENAME_LENGTH		128
#define MAX_MOVIE_COUNT			100
#define MAX_MOVIE_FRAMES		2048

/**
* GLOBAL VARIABLES
*/
int seed = -1;
int leech = -1;
int specPort = -1;
int specServer = -1;
int killServer = -1;

int main(int argc, char* argv[]){
	
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	// Option stuff
	int c;
	extern int optind, opterr;
	extern char *optarg;
	
	// Sockets, servers
	int tcp_port;
	char *serv_host_addr = (char*)malloc(sizeof(char)*20);
	
	/* ------------------------------------------------------------------------ */
	
	// Parse command line options
	while((c = getopt (argc, argv, "slhkp:a:")) != EOF) {
	    switch(c){
			case 's': // seeding mode
				seed = 0;
				break;
			case 'l': // leeching mode
				leech = 0;
				break;
			case 'p': // specify port
				tcp_port = atoi(optarg);
				specPort = 0;
				break;
			case 'a': // specify server ip
                strcpy(serv_host_addr, optarg);
				//serv_host_addr = optarg;
				specServer = 0;
				break;
			case 'k': // kill server
				killServer = 0;
				break;
			default:
		    case '?': // opt error
		    case 'h': // help
		      	help();
				exit(1);
	    }
  	}
	
	if((seed < 0 && leech < 0 && killServer < 0) ||
	(seed == 0 && leech == 0 && killServer < 0)){
		perror("Sepcify either of -s or -l\n");
		help();
		exit(1);
	}
	
	// DEFAULT PORT/ADDRESS
	if(specPort != 0){tcp_port = DEFAULT_PORT;}
	if(specServer != 0){serv_host_addr = DEFAULT_ADDRESS;}

	if(seed == 0 && killServer < 0){
		startSeeding(serv_host_addr, tcp_port);
	}else if(leech == 0 || killServer == 0){
		startLeeching(serv_host_addr, tcp_port);
	}
	
	return 0;
}

/**
* Start the movie leeching routine
*/
void startLeeching(char* address, int port)
{
	
	/* ------------------------------------------------------------------------ */
	/*								VARIABLES									*/
	/* ------------------------------------------------------------------------ */
	
	char input[MAX_INPUT_LENGTH];		// user input stdin
	bzero(input, MAX_INPUT_LENGTH);		// clear memory
	
	MovieFile* movies = (MovieFile*)malloc(sizeof(MovieFile) * MAX_MOVIE_COUNT);
	MovieFile movieSelected;
	int movies_received = 0;
	int movie_selected = 0;
	int curFrame; // frame ID currently being played
	
	Client* framesClient = (Client*)malloc(sizeof(Client) * MAX_MOVIE_FRAMES);
	
	// Socket stuff
	int sock, bytes;
	char* message = (char*)malloc(sizeof(char)*MAX_MSG_SIZE);
	memset(message, '\0', MAX_MSG_SIZE);
	
	/* ------------------------------------------------------------------------ */
	/*							CONNECT TO SERVER								*/
	/* ------------------------------------------------------------------------ */
	
	sock = connectToTracker(address, port);
	
	if(killServer == 0){
		// Tell server we want it to die
		identifyAsKiller(sock);
		close(sock);
		free(message);
		free(movies);
		free(framesClient);
		exit(0);
	}else{
		// Tell server we're a leecher
		identifyAsLeecher(sock);
		
		print_header();
		
		// Get server response with listing of movies
		if((bytes = read(sock, movies, sizeof(MovieFile) * MAX_MOVIE_COUNT)) < 0){
			perror("Error receiving movie listing from tracker.");
			exit(1);
		}
		
		printf("Movies available on the network:\n");
		
		while(movies->movieType > 0){
			movies_received++;
			printf("%d) %s  \t%2.1f sec  \t%d seeds\n", movies_received, movies->movieName, movies->duration, movies->seederCount);
			movies++;
		}
		movies -= movies_received;

		printf("\nPlease select a movie to stream by entering a number from 1 to %d or type 'exit' to quit\n", movies_received);
		
		while(movie_selected < 1 || movie_selected > movies_received){
			scanf("%s", input);
			if(strcmp(input, "exit")==0){exit(0);}
			movie_selected = atoi(input);
			if(movie_selected < 1 || movie_selected > movies_received){
				printf("Invalid movie selection, type a number from 1 to %d\n", movies_received);
			}
		}
		
		movieSelected = movies[movie_selected-1];
		
		if(write(sock, movieSelected.movieName, sizeof(movies[movie_selected-1].movieName)) <= 0){
			perror("Error while asking tracker for movie.");
			exit(1);
		}

		// Get listing of seeders to contact
		if((bytes = read(sock, framesClient, sizeof(Client) * MAX_MOVIE_FRAMES)) <= 0){
			perror("Error receiving digest from tracker.");
			exit(1);
		}
		
		close(sock);
		
		curFrame = 1;
		while(framesClient->port > 0 || curFrame <= movieSelected.originalFrameCount){
			
			/*
			* If frame is available somewhere, request it
			* otherwise just skip to next available frame!
			*/
			if(framesClient->port > 0){
				// Print the frame
				requestFrame(framesClient->address, framesClient->port, movieSelected.movieName, curFrame);
				// Print movie info
				printf("\n[Time remaining: %2.1f sec] [frame %d/%d] ", 
					((double)movieSelected.originalFrameCount-(double)curFrame)/(double)movieSelected.FPS, 
					curFrame, 
					movieSelected.originalFrameCount
				);
				printSlider(curFrame, movieSelected.originalFrameCount);
				usleep(1000000 / movieSelected.FPS);
			}
			framesClient++;
			curFrame++;
		}
		
		// Restart!
		startLeeching(address, port);
	}
}

/**
* Start the movie seeding routing
*/
void startSeeding(char* address, int port)
{
	
	print_header();
	
	/* ------------------------------------------------------------------------ */
	/*							VAR DECLARATIONS								*/
	/* ------------------------------------------------------------------------ */
	
	int movie_count = 0, i, pid;
	MovieFile movieFiles[MAX_MOVIE_COUNT];
	DIR* dp;
	struct dirent* d;
	
	// Socket stuff
	int sock, newsock, bytes;
	char* message = (char*)malloc(sizeof(char)*MAX_MSG_SIZE);
	memset(message, '\0', MAX_MSG_SIZE);
	FrameRequest* req = (FrameRequest*)malloc(sizeof(FrameRequest)*2);
	
	// Randomize port selection
    srand(time(NULL));
	int seeding_port = rand()%40000+7000; // random port from 7000 to 47000
    int seedingPort[1];
    seedingPort[0] = seeding_port;
	
	/* ------------------------------------------------------------------------ */
	/*							SCAN MOVIE FILES								*/
	/* ------------------------------------------------------------------------ */
	
	if(pathExists(MOVIE_FOLDER) < 0){
		printf("You need to have a movie folder '%s' containing ascii movie files.\n", MOVIE_FOLDER);
		exit(1);
	}
	
	dp = opendir(MOVIE_FOLDER);
	if(dp == NULL){
		printf("You need to have a movie folder '%s' containing ascii movie files.\n", MOVIE_FOLDER);
		exit(1);
	}else{
		d = readdir(dp);
		while(d){
			if(
				strcmp(d->d_name, ".DS_Store")!=0 && 
				strcmp(d->d_name, "movies.txt")!=0 && // that's not a movie file!
				strcmp(d->d_name, ".")!=0 &&	// ignore .
				strcmp(d->d_name, "..")!=0		// ignore ..
			){
				ReadMovieFile(getPath(MOVIE_FOLDER, d->d_name), &movieFiles[movie_count]);
				movieFiles[movie_count].seedingPort = seeding_port;
				movie_count++;
			}
			d = readdir(dp);
		}
	}
	
	/* ------------------------------------------------------------------------ */
	/*							DISPLAY MOVIE LIST								*/
	/* ------------------------------------------------------------------------ */
	printf("Movies available on this computer:\n");
	
	for(i=0; i<movie_count; i++){
		// Movie # and name
		printf("%d/%d: %s", i+1, movie_count, movieFiles[i].movieName);
		// Movie duration
		printf("\t%2.2f sec", movieFiles[i].duration);
		// Movie FPS
		printf("\t%d fps", movieFiles[i].FPS);
		// Frames available
		printf("\t%d/%d\t(%2.0f%%)\n", movieFiles[i].currentFrameCount, movieFiles[i].originalFrameCount, movieFiles[i].availability);
	}
	
	/* ------------------------------------------------------------------------ */
	/*						CONNECT TO TRACKING SERVER							*/
	/* ------------------------------------------------------------------------ */
	
	sock = connectToTracker(address, port);
	identifyAsSeeder(sock);
	waitForReady(sock);
	
	// Send our MovieFile array
	if(write(sock, movieFiles, sizeof(MovieFile) * movie_count) <= 0){
		perror("Error while sending movie list.");
		exit(1);
	}
	
	waitForReady(sock);
    
	// Send our random port
	if(write(sock, seedingPort, sizeof(seedingPort)+1) <= 0){
		perror("Error while sending random port.");
		exit(1);
	}
	
	// Close connection to tracker
	close(sock);
	
	printf("\nMovie list sent to tracker.\n");
	
	/* ------------------------------------------------------------------------ */
	/*								SEED MOVIES									*/
	/* ------------------------------------------------------------------------ */
	
	printf("Seeding movies on port %d. Waiting for requests...\n", seeding_port);
	
	sock = bindLocalAddress(seeding_port);
	listen(sock, MAX_CONNECTIONS);
	
	while(0==0){
		newsock = acceptConnection(sock);
		
		if((bytes = read(newsock, req, sizeof(FrameRequest)*2)) < 0){
			printf("Error in read.\n");
			free(req);
			free(message);
			exit(0);
		}
		
		// Is it a command to die?
		if(req->frameID == -666){
			printf("Shutting down...\n");
			free(req);
			free(message);
			exit(0);
		}
		
		pid = fork();

		if(pid == 0){
			handleFrameRequest(newsock, req, movieFiles, movie_count);
			free(message);
			free(req);
			exit(0);
		}else{
			wait(NULL);
		}
		
		close(newsock);
	}
	
	free(req);
	free(message);
	exit(0);
}

/**
* Tells the tracker that we're a leecher
*/
void identifyAsLeecher(int socket)
{
	write(socket, "leech", 6);
}

/**
* Tells the tracker that we're a seeder
*/
void identifyAsSeeder(int socket)
{
	write(socket, "seed", 5);
}

/**
* Tells the tracker that we're a killer
*/
void identifyAsKiller(int socket)
{
	write(socket, "kill", 5);
}

/**
* Prints a slider representing movie progress
*/
void printSlider(int current, int max)
{
	int now = (current*20)/max;
	int i;
	printf("[");
	for(i=1; i<=20; i++){
		if(i-1 <= now){printf("|");}else{printf(" ");}
	}
	printf("]\n");
}

/**
* Accept a connection on given socket, returns a new socket
*/
int acceptConnection(int sock)
{
	int newsock;
	unsigned int clilen;
	struct sockaddr_in cli_addr;
	
	clilen = sizeof(cli_addr);
	newsock = accept(sock, (struct sockaddr *) &cli_addr, &clilen);
	if (newsock < 0) {
		perror("Error while accepting connection.\n");
		exit(1);
	}
	
	return newsock;
}

/**
* Waits until peer is ready
*/
void waitForReady(int sock)
{
	char* message = (char*)malloc(sizeof(char) * MAX_MSG_SIZE);
	int bytes;
	
	// Get server response with listing of movies
	if((bytes = read(sock, message, MAX_MSG_SIZE)) < 0){
		printf("Error in read.\n");
		free(message);
		exit(0);
	}
	
	if(strcmp(message, "ready")!=0){
		printf("Peer is not ready for us, try again later.\n");
		free(message);
		exit(0);
	}
	
	free(message);
}

/**
* Submits given frame to a leecher
*/
void handleFrameRequest(int sock, FrameRequest* req, MovieFile movies[], int movieCount)
{
	char frame[50000];
	int i, found = 0;
	
	// Find movie requested
	for(i=0; i<movieCount; i++){
		if(strcmp(req->movieName, movies[i].movieName)==0){
			found = getFrame(getPath(MOVIE_FOLDER, movies[i].filename), req->frameID, frame);
		}
	}
	
	if(!found){
		printf("Couldn't find requested frame %d for movie '%s'\n", req->frameID, req->movieName);
	}else{
		printf("Sent frame %d of movie '%s'\n", req->frameID, req->movieName);
	}
	
	// Submit
	write(sock, frame, strlen(frame)*sizeof(char)+1);
}

/**
* Requests a movie frame from a seeder and prints it
*/
void requestFrame(char* address, int port, char* movieName, int frameID)
{
	int sock = connectToTracker(address, port);
	int bytes;
	char* message = (char*)malloc(sizeof(char)*MAX_MSG_SIZE);
	
	FrameRequest req[1];
	strcpy(req[0].movieName, movieName);
	req[0].frameID = frameID;
	
	// Write
	write(sock, req, sizeof(FrameRequest));
	// Read
	if((bytes = read(sock, message, MAX_MSG_SIZE)) < 0){
		printf("Error in read.\n");
		free(message);
		exit(0);
	}
	
	close(sock);
	
	// Print frame
	system("clear");
	printf("%s", message);
	free(message);
}

/**
* Binds to local address to let others connect to this program
* return a socket descriptor
*/
int bindLocalAddress(int port)
{
	// Socket stuff
	int sock;
	struct sockaddr_in serv_addr;
	
	// Create socket from which to read
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
	   perror("Can't open stream socket.\n");
	   exit(1);
	}
	
	// bind our local address so client can connect to us
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(port);
	if(bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("Can't bind to local address.\n");
		exit(1);
	}
	
	return sock;
}

/**
* Creates a connection to tracking server and returns socket descriptor
*/
int connectToTracker(char* address, int port)
{
	// Socket stuff
	int sock;
	struct sockaddr_in serv_addr;
	struct hostent *hp;
	
	/*
	 * First try to convert the host name as a dotted-decimal number.
	 * Only if that fails do we call gethostbyname().
	 */
	bzero((void *) &serv_addr, sizeof(serv_addr));
	if ((hp = gethostbyname(address)) == NULL) {
	  perror("Host name error.\n");
	  exit(1);
	}
	
	bcopy(hp->h_addr, (char *) &serv_addr.sin_addr, hp->h_length);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);
	
	/* open a TCP socket (an Internet stream socket). */
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Can't open stream socket.\n");
		exit(1);
	}
	
	/* socket open, so connect to the server */
	if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("Can't connect to tracking server or peer.\n");
		exit(1);
	}
	
	return sock;
}

/**
* Print a help message
*/
void help()
{
	printf("client - thomas.lextrait@gmail.com\n");
	printf("usage: client [-s|-l|-k] [-a<server ip>] [-p<port>]\n");
	printf("\tEither -s or -l need to be used but not both.\n");
	printf("\t-s\tStart in seeding mode.\n");
	printf("\t-l\tStart in leeching mode.\n");
	printf("\t-k\tKill the server.\n");
	printf("\t-a\tSpecify a tracking server ip address, otherwise will use '%s'.\n", DEFAULT_ADDRESS);
	printf("\t-p\tSpecify a tracking server port, otherwise will use %d\n", DEFAULT_PORT);
	printf("\t-h\tDisplay this help message.\n");
}
