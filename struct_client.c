/**
* @file struct_client.c
* @author Thomas Lextrait
*/

#include "struct_client.h"

/**
* Allows tracker to make a request to die to a seeder
*/
typedef struct FrameRequest{
	char movieName[128];
	int frameID;
}FrameRequest;

#ifndef INADDR_NONE
#define INADDR_NONE 0xffffffff /* should be in <netinet/in.h> */
#endif

#define MAX_MSG_SIZE	256

/**
* Initialize a client struct
*/
void initializeClient(Client* client, int socket)
{
	client->clientType = client_undefined;
	client->socket = socket;
	client->port = 0;
	client->clientID = -1;
}

/**
* Tells a client to go die in a fire
*/
void killClient(Client client)
{
	// Socket stuff
	int sock;
	struct sockaddr_in serv_addr;
	struct hostent *hp;
	
	FrameRequest killReq[1];
	killReq[0].frameID = -666;
	
	/*
	 * First try to convert the host name as a dotted-decimal number.
	 * Only if that fails do we call gethostbyname().
	 */
	bzero((void *) &serv_addr, sizeof(serv_addr));
	if ((hp = gethostbyname(client.address)) == NULL) {
	  perror("Host name error.\n");
	  exit(1);
	}
	
	bcopy(hp->h_addr, (char *) &serv_addr.sin_addr, hp->h_length);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(client.port);
	
	/* open a TCP socket (an Internet stream socket). */
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Can't open stream socket.\n");
		exit(1);
	}
	
	/* socket open, so connect to the server */
	if(connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) >= 0){
		// Kill command
		write(sock, killReq, sizeof(killReq));
		close(sock);
	}else{
		printf("Could not kill %s:%d\n", client.address, client.port);
	}
}
