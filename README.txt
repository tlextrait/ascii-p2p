CS4513 - Distributed Systems
Project 3

Thomas Lextrait
tlextrait@wpi.edu

Language:
C

Compilation:
Use make

Bugs:
Doesn't work when clients are on different computers, too much data loss.

Server:
usage: server [-h] [-p<port>]
	-p	Specify a port, otherwise will use 6666.
	-h	Display this help message.

Examples:
./server
./server -p 8763

Client:
usage: client [-s|-l|-k] [-a<server ip>] [-p<port>]
	Either -s or -l need to be used but not both.
	-s	Start in seeding mode.
	-l	Start in leeching mode.
	-k	Kill the server.
	-a	Specify a tracking server ip address, otherwise will use '192.168.1.108'.
	-p	Specify a tracking server port, otherwise will use 6666
	-h	Display this help message.

Examples:

Start a leeching client
./client -l -a 192.168.1.116 -p 5433

Start a seeding client
./client -s -a 192.168.1.88

Kill the server and seeders
./client -k -a 192.168.1.79
